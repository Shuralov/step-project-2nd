STEP-PROJECT-2nd
DAN-IT
Step Project ["Forkio"](https://astakoro.gitlab.io/step-project-2/index.html) 

Studens

- Shuralov Roman(Revolutionary-editor)
- Korostylova Anastasiia (developer1)



Technologies


- Gulp
- HTML5/SCSS/JS
- Gitlab Pages


Revolutionary-editor

- Section "Revolutionary Editor", "Here is what you get", "Fork Subscription Pricing", Git, buttons: watch/star/fork.



Developer1

- Header, Dropdown menu, Section "People Are Talking About Fork", GitLab Pages


Team-work

- Gulpfile, readme file,  code review, merge branches into master