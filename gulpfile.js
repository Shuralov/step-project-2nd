const {src, dest, series, parallel, watch} = require('gulp');
const browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    uncss = require('postcss-uncss'),
    del = require('del'),
    minify = require('gulp-minify'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    useref = require('gulp-useref'),  //для изменения положнеия css и js
    rep = require('gulp-replace-image-src');

function emptyDist() {
    return del('./dist/**');
}

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyHtmlRoot() {
    return src('./dist/index.html')
        .pipe(rep({
            prependSrc : './dist/assets/img/',   //меняет размещение картинок на ./dist/assets/
            keepOrigin : false
        }))
        .pipe(useref())
        .pipe(dest('./'))
}

function buildJs() {
    return src('./src/js/*.js')
        .pipe(minify({noSource: true}))
        .pipe(concat('all.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true }));
}

function buildCss() {
    return src('./src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['src/index.html']})
        ]))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(cssnano())
        .pipe(concat('all.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'));
}

function serve() {
    browserSync.init({
        server: './dist'
    });
    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildCss);
    watch(['./src/js/*.js'], buildJs);
    watch(['./src/assets/**/**/'], copyAssets);

}

function imagesMinifier() {
    return src('src/assets/img/**')
        .pipe(imagemin())
        .pipe(dest('./dist/assets/img/'))
}

exports.html = copyHtml;
exports.htmlRoot = copyHtmlRoot;
exports.scss = buildCss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = buildJs;
exports.serve = serve;
exports.imgmin = imagesMinifier;

exports.build = series(
    emptyDist,
    parallel(
        series(buildCss, buildJs, imagesMinifier, copyAssets, copyHtml)
    ),
    copyHtmlRoot
);


exports.dev = serve;

exports.default = () => src(
    "style.scss"
).pipe(sass()).pipe(dest("./"));